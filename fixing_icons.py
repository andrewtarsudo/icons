from pathlib import Path
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import ElementTree, Element
from cairosvg import svg2pdf


def resize_icons(dirpath: str | Path, width: int, height: int):
    for item in Path(dirpath).iterdir():
        etree: ElementTree = ET.parse(item)
        root: Element = etree.getroot()
        # _width: str = root.get("width")
        # _height: str = root.get("height")
        root.set("width", f"{width}")
        root.set("height", f"{height}")
        etree.write(item)
        print(f"Success! File {item.name}")


def convert(dirpath: str | Path):
    for item in Path(dirpath).iterdir():
        svg2pdf(url=str(item), write_to=str(item.with_suffix(".png")))


if __name__ == '__main__':
    _dirpath: str = r"C:\Users\tarasov-a\Desktop\themify-icons-font\themify-icons\SVG"
    # resize_icons(_dirpath, 150, 150)
    convert(_dirpath)
